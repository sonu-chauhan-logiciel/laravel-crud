@extends('../admin.layout.master')
@section('title', 'Post Image Edit')
@section('content')
    <section class="main_section">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif 
        <div class="col-xl-12">
            <div class="card mt-4">
                <div class="card-header">
                    <h4 class="text-info">Post Image Edit</h4>
                </div>
                <div class="card-body">
                    <div class="form_area">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="row form" action="{{ route('posts.image.update', ['id' => $post->id])}}" method="POST" enctype="multipart/form-data">
                                    {{method_field('PUT')}}
                                    {{ csrf_field() }}
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name">Image Upload</label>
                                            <input class="form-control" type="file" name="image">
                                            <img src="{{ asset('blog_images/'.$post->img_name) }}" alt="..." style="height: 50px; width: 100px; margin-top: 10px;">
                                            @error('image')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="email">Status</label>
                                            <label class="text-muted d-flex align-items-center mt-2">
                                                <input type="Radio" name="status" class="mr-1 ml-2" value="1" 
                                                    @if($post->status == 1)
                                                        {{'checked'}}
                                                    @endif
                                                >Enable
                                                <input type="Radio" name="status" class="mr-1 ml-2" value="0"
                                                    @if($post->status == 0)
                                                        {{'checked'}}
                                                    @endif
                                                >Disable
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" style="margin-top:30px;">
                                            <input type="submit" class="btn btn-primary" value="Submit">
                                            <a href="{{ route('posts.index') }}" class="btn btn-success">Back</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="form_section">
            <!--Form Section Start-->
            <!--<div class="form_area">
                <div class="row">
                    <div class="col-md-12">
                        <form class="row form" action="{{ route('posts.image.update', ['id' => $post->id])}}" method="POST" enctype="multipart/form-data">
                            {{method_field('PUT')}}
                            {{ csrf_field() }}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name">Image Upload</label>
                                    <input class="form-control" type="file" name="image">
                                    <img src="{{ asset('blog_images/'.$post->img_name) }}" alt="..." style="height: 50px; width: 100px; margin-top: 10px;">
                                    @error('image')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="email">Status</label>
                                    <select name="status" class="form-control">
                                        @php
                                            $value = $post->status;
                                        @endphp
                                        <option value="1"
                                            @php
                                                if($value == 1){
                                                    echo 'selected';
                                                }
                                            @endphp
                                        >Enable</option>
                                        <option value="0"
                                            @php 
                                                if($value == 0){
                                                    echo 'selected';
                                                }
                                            @endphp
                                        >Disable</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" style="margin-top:30px;">
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>-->
            <!--Form Section End-->
        </section>
    </section>
@endsection