@extends('../admin.layout.master')
@section('title', 'Create Post')
@section('content')

	<section class="main_section">
		@if(session()->has('message'))
		    <div class="alert alert-success">
		        {{ session()->get('message') }}
		    </div>
		@endif 
    	<div class="col-xl-12">
			<div class="card mt-4">
				<div class="card-header d-flex justify-content-between align-items-center">
					<h4 class="text-info">Create Post</h4>
				</div>
				<div class="card-body">
		    		<div class="container">
		    			<div class="row">
		    				<div class="col-xl-12">
		    					<div class="form_area">
		    					    <form action="{{ route('posts.store') }}" method="POST" enctype="multipart/form-data">
		    					        {{ csrf_field() }}
		    					        <div class="row">
		    					        	<div class="col-xl-12">
			    					            <div class="form-group">
			    					                <label>Post Title:</label>
			    					                <input type="text" class="form-control" name="name" value="{{ old('name') }}">
			    					                @error('name')
			    					                    <div class="text-danger">{{ $message }}</div>
			    					                @enderror
			    					            </div>
		    					        	</div>
		    					        </div>
		    					        <div class="row">
		    					        	<div class="col-xl-12">
			    					            <div class="form-group">
			    					                <label>Short Description:</label>
			    					                <input type="text" class="form-control" name="short_description" value="{{ old('short_description') }}">
			    					                @error('short_description')
			    					                    <div class="text-danger">{{ $message }}</div>
			    					                @enderror
			    					            </div>
		    					        	</div>
		    					        </div>
		    					        <div class="row">
		    					        	<div class="col-xl-12">
			    					            <div class="form-group">
			    					                <label>Description:</label>
			    					                <textarea id="content" class="form-control" rows="4" col="5" name="description" ></textarea>
			    					                @error('description')
			    					                    <div class="text-danger">{{ $message }}</div>
			    					                @enderror
			    					            </div>
		    					        	</div>
		    					        </div>
		    					        <div class="row">
		    					        	<div class="col-xl-12">
			    					            <div class="form-group">
					                            	<label class="text-muted d-flex align-items-center">
					                            		Status:
					                            		<input type="radio" name="status" class="mr-1 ml-2" value="1" @if(old('status')=='1')) checked @endif>Published
					                            		<input type="radio" name="status" class="mr-1 ml-2" value="0" @if(old('status')=='0')) checked @endif>Unpublished
					                				</label>
					                                @error('status')
					                                    <div class="text-danger">{{ $message }}</div>
					                                @enderror
			    					            </div>
		    					        	</div>
		    					        </div>
		    					        <div class="row">
		    					        	<div class="col-xl-12">
			    					            <div class="form-group">
			    					                <input type="submit" class="btn btn-info" name="submit" value="Save">
			    					                <a href="{{ route('posts.index') }}" class="btn btn-success">Back</a>
			    					            </div>
			    					        </div>
		    					        </div>
		    					    </form>
		    					</div>
		    				</div>
		    			</div>
		    		</div>
				</div>
			</div>
	</section>

@endsection