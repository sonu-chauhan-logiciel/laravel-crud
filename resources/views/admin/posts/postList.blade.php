@extends('../admin.layout.master')
@section('title', 'Post Listing')
@section('content')

	<section class="main_section">
		@if(session()->has('message'))
		    <div class="alert alert-success">
		        {{ session()->get('message') }}
		    </div>
		@endif 
    	<div class="col-xl-12">
			<div class="card mt-4">
				<div class="card-header d-flex justify-content-between align-items-center">
					<h4 class="text-info">Post Listing</h4>
					<a href="{{ route('posts.create') }}" class="btn btn-info">Create Post</a>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Id</th>
									<th>Name</th>
									<th>Description</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($posts as $post)
									<tr>
										<td>{{ $post->id }}</td>
										<td>{{ $post->post_name }}</td>
										<td>{{ Illuminate\Support\Str::limit($post->short_description, 50) }}</td>
										<td>
											@if($post->status ==1)
												{{'Active'}}
											@else
												{{'Deactive'}}
											@endif
										</td>
										<td>
											<a href="{{ route('posts.edit', ['id' => $post->id]) }}" class="btn btn-info">
												<i class="fa fa-pencil"></i>
											</a>
											<!--<a href="{{ route('posts.view', ['id' => $post->id]) }}" class="btn btn-primary">
												<i class="fa fa-eye"></i>
											</a>-->
											<form method="POST" action="{{ route('posts.delete', ['id' => $post->id]) }}" style="margin-right: 5px; margin-top:5px;">
											    {{ csrf_field() }}
											    {{ method_field('DELETE') }}
											    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></button>
											</form>
											<a href="{{ route('posts.image.listing', ['id' => $post->id]) }}"class="btn btn-info">
												Add Image
											</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table> 
						{{ $posts->links() }}
					</div>
				</div>
			</div>
    		<!--<div class="top_bar">
				<div class="left">
				</div>
				<div class="right">
					
				</div>
    		</div>-->
			
    	</div>
	</section>
@endsection