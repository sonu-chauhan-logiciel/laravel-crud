@extends('../admin.layout.master')
@section('title', 'Post image')
@section('content')
    <section class="main_section">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif 
        <div class="col-xl-12">
            <div class="card mt-4">
                <div class="card-header">
                    <h4 class="text-muted">Add Post Image</h4>
                </div>
                <div class="card-body">
                    <div class="form_area">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="row form" action="{{-- route('posts.image.save', ['id' => $post->id]) --}}" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="name">Image Upload</label>
                                            <input class="form-control" type="file" name="image">
                                            @error('image')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Feature Image</label>
                                            <label class="text-muted d-flex align-items-center mt-3">
                                                <input type="hidden" name="featureImage" value="0">
                                                <input type="checkbox" name="featureImage" value="1">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="email">Status</label>
                                            <label class="text-muted d-flex align-items-center mt-2">
                                                <input type="radio" name="status" class="mr-1 ml-2" value="1" @if(old('status')=='1')) checked @endif>Enable
                                                <input type="radio" name="status" class="mr-1 ml-2" value="0" @if(old('status')=='0')) checked @endif>Disable
                                            </label>
                                            @error('status')
                                                <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group" style="margin-top:30px;">
                                            <input type="submit" class="btn btn-primary" value="Submit">
                                            <a href="{{ route('posts.index') }}" class="btn btn-success">Back</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-12">
            <div class="card mt-4">
                <div class="card-header">
                    <h4 class="text-muted">View Images</h4>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Image</th>
                                <th>Feature Image</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    <td>{{ $post->id }}</td>
                                    <td>
                                        <img src="{{ asset('blog_images/'.$post->images) }}" alt="..." style="height:30px; width: 50px;">
                                    </td>
                                    <td>
                                        @if($post->feature_image == 1)
                                            {{'Active'}}
                                        @endif
                                    </td>
                                    <td>
                                        @if($post->status == 1)
                                            {{'Enable'}}
                                        @else
                                            {{'Disable'}}
                                        @endif
                                    </td>
                                    <td>
                                        <!--<a href="{{-- route('posts.image.edit', ['id' =>$post->id]) --}}" class="btn btn-info">
                                            <i class="fa fa-pencil"></i>
                                        </a>-->
                                        <form method="POST" action="{{ route('posts.image.delete', ['id' => $post->id]) }}" style="margin-right: 5px; margin-top:5px;">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection