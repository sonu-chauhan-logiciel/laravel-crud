@extends('admin.layout.master')
@section('title', 'Post Comments')
@section('content')

	<section class="main_section">
		@if(session()->has('message'))
		    <div class="alert alert-success">
		        {{ session()->get('message') }}
		    </div>
		@endif 
    	<div class="col-xl-12">
			<div class="card mt-4">
				<div class="card-header">
					<h4 class="text-info">Comments Listing</h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Id</th>
									<th>Comments</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($comments as $comment)
									<tr>
										<td>{{ $comment->id }}</td>
										<td>{!! Illuminate\Support\Str::limit($comment->content, 50) !!}</td>
										<td>
											@if($comment->status == 1)
												{{'Published'}}
											@else
												{{'Unpublished'}}
		            	            		@endif
										</td>
										<td>
											<a href="{{ route('comments.edit', ['id' => $comment->id]) }}" class="btn btn-info">
												<i class="fa fa-pencil"></i>
											</a>
											<form method="POST" action="{{ route('comments.delete', ['id' => $comment->id]) }}" style="margin-right: 5px;">
											    {{ csrf_field() }}
											    {{ method_field('DELETE') }}
											    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></button>
											</form>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
						{{ $comments->links() }} 
					</div>
				</div>
			</div>
    		<!--<div class="top_bar">
				<div class="left">
					<h4>
						Comments List
					</h4>
				</div>
    		</div>
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Id</th>
							<th>Comments</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($comments as $comment)
							<tr>
								<td>{{ $comment->id }}</td>
								<td>{{ Illuminate\Support\Str::limit($comment->content, 50) }}</td>
								<td>
									<select name="status" class="btn btn-primary">
									    @php
									        $value = $comment->status;
									    @endphp
									    <option value="1"
									        @php
									            if($value == 1){
									                echo 'selected';
									            }
									        @endphp
									    >Enable</option>
									    <option value="0"
									        @php 
									            if($value == 0){
									                echo 'selected';
									            }
									        @endphp
									    >Disable</option>
									</select>
								</td>
								<td>
									<a href="{{ route('comments.edit', ['id' => $comment->id]) }}" class="btn btn-info">
										<i class="fa fa-pencil"></i>
									</a>
									<form method="POST" action="{{ route('comments.delete', ['id' => $comment->id]) }}" style="margin-right: 5px;">
									    {{ csrf_field() }}
									    {{ method_field('DELETE') }}
									    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
									</form>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table> 
			</div>-->
    	</div>
	</section>
@endsection