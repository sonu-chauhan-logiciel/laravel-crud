@extends('admin.layout.master')
@section('title', 'Edit Comment')
@section('content')

	<section class="main_section">
    	<div class="col-xl-12">
            <div class="card mt-4">
                <div class="card-header">
                    <h4 class="text-info">Comment Editing</h4>
                </div>
                <div class="card-body">
                    <div class="form_area">
                        <form action="{{ route('comments.update', ['id' => $comment->id]) }}" method="POST" enctype="multipart/form-data">
                            {{method_field('PUT')}}
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="form-group">
                                        <label>Comments:</label>
                                        <textarea id="content" class="form-control" rows="4" col="5" name="content">
                                        {{$comment->content}}
                                        </textarea>
                                        @error('content')
                                            <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="form-group">
                                        <label>Status:</label>
                                        <label class="text-muted d-flex align-items-center">
                                            <input type="Radio" name="status" class="mr-1 ml-2" value="1" 
                                                @if($comment->status == 1)
                                                    {{'checked'}}
                                                @endif
                                            >Published
                                            <input type="Radio" name="status" class="mr-1 ml-2" value="0"
                                                @if($comment->status == 0)
                                                    {{'checked'}}
                                                @endif
                                            >Unpublished
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-info" name="submit" value="Update">
                                        <a href="{{ route('comment.index') }}" class="btn btn-success">Back</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>    
                </div>
            </div>
    		<!--<div class="top_bar">
				<div class="left">
					<h2 class="text-info">
						Edit Comment
					</h2>
				</div>
				<div class="right">
					<a href="{{ route('comment.index') }}" class="btn btn-success">Back</a>
				</div>
    		</div>-->
    	</div>
    	<!--<section class="form_section">
    		<div class="container">
    			<div class="row">
    				<div class="col-xl-12">
    					<div class="form_area">
    					    <form action="{{ route('comments.update', ['id' => $comment->id]) }}" method="POST" enctype="multipart/form-data">
    					    	{{method_field('PUT')}}
    					        {{ csrf_field() }}
    					        <div class="row">
    					        	<div class="col-xl-6">
	    					            <div class="form-group">
	    					                <label>Comments:</label>
	    					                <textarea class="form-control" rows="4" col="5" name="content">
	    					                {{$comment->content}}
	    					                </textarea>
	    					                @error('description')
	    					                    <div class="text-danger">{{ $message }}</div>
	    					                @enderror
	    					            </div>
    					        	</div>
    					        </div>
    					        <div class="row">
    					        	<div class="col-xl-6">
	    					            <div class="form-group">
	    					                <label>Status:</label>
	    					                <select class="form-control" name="status">
	    					                	@php
    						                		$value = $comment->status;
    						                	@endphp
    						                	<option value="1"
    						                		@php
    						                			if($value == 1){
    						                				echo 'selected';
    						                			}
    												@endphp
    						                	>Published</option>
    						                	<option value="0"
    	            		                		@php 
    	            		                			if($value == 0){
    	            		                				echo 'selected';
    	            		                			}
    	            								@endphp
    						                	>Unpublished</option>
	    					                </select>
	    					                @error('status')
	    					                    <div class="text-danger">{{ $message }}</div>
	    					                @enderror
	    					            </div>
    					        	</div>
    					        </div>
    					        <div class="row">
    					        	<div class="col-xl-6">
	    					            <div class="form-group">
	    					                <input type="submit" class="btn btn-info" name="submit" value="Add">
	    					            </div>
	    					        </div>
    					        </div>
    					    </form>
    					</div>
    				</div>
    			</div>
    		</div>
    	</section>-->
	</section>

@endsection