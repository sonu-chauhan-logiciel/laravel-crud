@extends('../admin.layout.master')
@section('title', 'Pages List')
@section('content')

	<section class="main_section">
		@if(session()->has('message'))
		    <div class="alert alert-success">
		        {{ session()->get('message') }}
		    </div>
		@endif 
    	<div class="col-xl-12">
			<div class="card mt-4">
				<div class="card-header d-flex justify-content-between align-items-center">
					<h4 class="text-info">Page Listing</h4>
					<a href="{{ route('pages.create') }}" class="btn btn-info">Create Page</a>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Id</th>
									<th>Name</th>
									<th>Description</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($pages as $page)
									<tr>
										<td>{{$page->id}}</td>
										<td>{{$page->name}}</td>
										<td>{{ Illuminate\Support\Str::limit($page->short_description, 50) }}</td>
										<td>
											@if($page->status ==1)
												{{'Published'}}
											@else
												{{'Unpublished'}}
											@endif
										</td>
										<td>
											<a href="{{ route('pages.edit', ['id' => $page->id]) }}" class="btn btn-info">
												<i class="fa fa-pencil"></i>
											</a>
											<a href="{{ route('pages.view', ['id' => $page->id]) }}" class="btn btn-primary">
												<i class="fa fa-eye"></i>
											</a>
											<form method="POST" action="{{ route('pages.delete', ['id' => $page->id]) }}" style="margin-right: 5px;">
											    {{ csrf_field() }}
											    {{ method_field('DELETE') }}
											    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></button>
											</form>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table> 
						{{-- $posts->links() --}}
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection