@extends('../admin.layout.master')
@section('title', 'Edit Page')
@section('content')

	<section class="main_section">
		@if(session()->has('message'))
		    <div class="alert alert-success">
		        {{ session()->get('message') }}
		    </div>
		@endif 
    	<div class="col-xl-12">
			<div class="card mt-4">
				<div class="card-header d-flex justify-content-between align-items-center">
					<h4 class="text-info">Create Page</h4>
				</div>
				<div class="card-body">
		    		<div class="container">
		    			<div class="row">
		    				<div class="col-xl-12">
		    					<div class="form_area">
		    					    <form action="{{ route('pages.update', ['id' => $page->id])}}" method="POST" enctype="multipart/form-data">
                                        {{method_field('PUT')}}
		    					    	{{ csrf_field() }}
		    					        <div class="row">
		    					        	<div class="col-xl-12">
			    					            <div class="form-group">
			    					                <label>Page Title:</label>
			    					                <input type="text" class="form-control" name="name" value="{{$page->name}}">
			    					                @error('name')
			    					                    <div class="text-danger">{{ $message }}</div>
			    					                @enderror
			    					            </div>
		    					        	</div>
		    					        </div>
		    					        <div class="row">
		    					        	<div class="col-xl-12">
			    					            <div class="form-group">
			    					                <label>Short Description:</label>
			    					                <input type="text" class="form-control" name="short_description" value="{{$page->short_description}}">
			    					                @error('short_description')
			    					                    <div class="text-danger">{{ $message }}</div>
			    					                @enderror
			    					            </div>
		    					        	</div>
		    					        </div>
		    					        <div class="row">
		    					        	<div class="col-xl-12">
			    					            <div class="form-group">
			    					                <label>Description:</label>
			    					                <textarea id="content" class="form-control" rows="4" col="5" name="description" >
														{{$page->description}}
			    					                </textarea>
			    					                @error('description')
			    					                    <div class="text-danger">{{ $message }}</div>
			    					                @enderror
			    					            </div>
		    					        	</div>
		    					        </div>
		    					        <div class="row">
		    					        	<div class="col-xl-12">
			    					            <div class="form-group">
					                            	<label class="text-muted d-flex align-items-center">
					                            		Status:
					                            		<input type="hidden" name="status" class="mr-1 ml-2" value="0">
					                            		<input type="checkbox" name="status" class="mr-1 ml-2" value="1" 
					                            		    @if($page->status == 1)
					                            		        {{'checked'}}
					                            		    @endif
					                            		>Published
					                				</label>
			    					            </div>
		    					        	</div>
		    					        </div>
		    					        <div class="row">
		    					        	<div class="col-xl-12">
			    					            <div class="form-group">
			    					                <input type="submit" class="btn btn-info" name="submit" value="Save">
			    					                <a href="{{ route('pages.index') }}" class="btn btn-success">Back</a>
			    					            </div>
			    					        </div>
		    					        </div>
		    					    </form>
		    					</div>
		    				</div>
		    			</div>
		    		</div>
				</div>
			</div>
	</section>

@endsection