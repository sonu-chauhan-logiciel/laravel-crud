@extends('../admin.layout.master')
@section('title', 'Laravel-Crud')
@section('content')
	<section class="main_section">
		<div class="container">
		    <div class="row">
		    	<div class="col-xl-12">
					<div class="card mt-4">
						<div class="card-header d-flex justify-content-between align-items-center">
							<h4 class="text-info">Page View</h4>
							<a href="{{ route('pages.index') }}" class="btn btn-success">Back</a>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<td>Page Id</td>
											<td>{{$page->id}}</td>
										</tr>
										<tr>
											<td>Page Name</td>
											<td>{{$page->name}}</td>
										</tr>
										<tr>
											<td>Short Description</td>
											<td>{{$page->short_description}}</td>
										</tr>
										<tr>
											<td>Description</td>
											<td>
												{!! $page->description!!}
											</td>
										</tr>
										<tr>
											<td>Page Status</td>
											<td>
	            	            				@if($page->status == 1)
													{{'Active'}}
												@else
													{{'Deactive'}}
			            	            		@endif
											</td>
										</tr>
									</tbody>
								</table>  
							</div>
						</div>
					</div>
		    	</div>
			</div>
		</div>
	</section>
@endsection