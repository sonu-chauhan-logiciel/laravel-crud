@extends('../admin.layout.master')
@section('title', 'Menus List')
@section('content')

	<section class="main_section">
		@if(session()->has('message'))
		    <div class="alert alert-success">
		        {{ session()->get('message') }}
		    </div>
		@endif 
    	<div class="col-xl-12">
			<div class="card mt-4">
				<div class="card-header d-flex justify-content-between align-items-center">
					<h4 class="text-info">Menu Listing</h4>
					<a href="{{ route('menus.create') }}" class="btn btn-info">Create Menu</a>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Id</th>
									<th>Name</th>
									<th>Status</th>
									<th>Order</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($menus as $menu)
									<tr>
										<td>{{$menu->id}}</td>
										<td>{{$menu->name}}</td>
										<td>
											@if($menu->status == 1)
												{{'Published'}}
											@else
												{{'Unpublished'}}
											@endif
										</td>
										<td>{{$menu->display_order}}</td>
										<td>
											<a href="{{ route('menus.edit', ['id' => $menu->id]) }}" class="btn btn-info">
												<i class="fa fa-pencil"></i>
											</a>
											<form method="POST" action="{{ route('menus.delete', ['id' => $menu->id]) }}" style="margin-right: 5px;">
											    {{ csrf_field() }}
											    {{ method_field('DELETE') }}
											    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></button>
											</form>
										</td>
										</tr>
								@endforeach	
							</tbody>
						</table> 
						{{-- $menus->links() --}}
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection