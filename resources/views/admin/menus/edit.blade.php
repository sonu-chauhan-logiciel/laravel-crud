@extends('../admin.layout.master')
@section('title', 'Edit Menu')
@section('content')

	<section class="main_section">
		@if(session()->has('message'))
		    <div class="alert alert-success">
		        {{ session()->get('message') }}
		    </div>
		@endif 
    	<div class="col-xl-12">
			<div class="card mt-4">
				<div class="card-header d-flex justify-content-between align-items-center">
					<h4 class="text-info">Edit Menu</h4>
				</div>
				<div class="card-body">
		    		<div class="container">
		    			<div class="row">
		    				<div class="col-xl-12">
		    					<div class="form_area">
		    					    <form action="{{ route('menus.update', ['id' => $menu->id]) }}" method="POST" enctype="multipart/form-data">
                                        {{method_field('PUT')}}
		    					    	{{ csrf_field() }}
		    					    	@include('admin.menus.form')
    	    					    </form>
		    					</div>
		    				</div>
		    			</div>
		    		</div>
				</div>
			</div>
	</section>

@endsection