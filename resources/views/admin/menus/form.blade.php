<div class="row">
	<div class="col-xl-12">
		<div class="form-group">
			<label>Menu:</label>
			<select name="parent_id" class="form-control">
				<option value="">Select Parent Menu</option>
				@if(isset($menu))
					@php
						$traverse = function ($menus, $prefix = '-') use (&$traverse, $menu) {
							foreach ($menus as $menuItem) {
						    	$select = ($menuItem->id == $menu->parent_id) ? 'selected': '';
					        	echo '<option value="'.$menuItem->id.'" '.$select.' >'.$prefix.' '.$menuItem->name.'</option>';
						        $traverse($menuItem->children, $prefix.'-');
						    }
						};
						$traverse($menus, '');
					@endphp
				@else
					@php
						$traverse = function ($menus, $prefix = '-') use (&$traverse) {
						    foreach ($menus as $menu) {
						        echo '<option value="'.$menu->id.'">'.$prefix.' '.$menu->name.'</option>';
						        $traverse($menu->children, $prefix.'-');
						    }
						};
						$traverse($menus, '');
					@endphp
				@endif
			</select>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xl-12">
		<div class="form-group">
		    <label>Sub Menu:</label>
		    <input type="text" class="form-control" name="name" 
		    value="@if(isset($menu->name)){{ $menu->name }}@else{{ old('name') }}@endif">
		    @error('name')
		        <div class="text-danger">{{ $message }}</div>
		    @enderror
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xl-12">
		<div class="form-group">
		    <label>Menu Type:</label>
		    @if(isset($menu))
		    	<input type="radio" name="menu_type" class="mr-1 ml-2" onclick="show_page();" value="Page"  @if($menu->ref_type == 'Page') {{ 'checked' }}@endif >Page
		    	<input type="radio" name="menu_type" class="mr-1 ml-2" onclick="show_url();" value="Url"  @if($menu->ref_type == 'Url') {{ 'checked' }}@endif >URL
		        <div class="row">
					<div class="col-xl-6 offset-xl-1">
		                <div class="page hide" id="page">
							<label>Page</label>
							<select class="form-control" name="page">
								<option value="">Select The Page</option>
								@foreach($pages as $page)
									<option value="{{$page->id}}"
										@if($page->id == $menu->ref_value)
											{{'selected'}}
										@endif
										>{{$page->name}}</option>
								@endforeach
							</select>
		                </div>
					</div>
		        </div>
		        <div class="row">
					<div class="col-xl-6 offset-xl-1">
		                <div class="url hide" id="url">
							<label class="d-flex align-items-center">
								URL:
								<input type="text" name="url" class="form-control ml-2" 
								@if($menu->ref_type == 'Url')
									value="{{$menu->ref_value}}" 
								@else
									value="" 
								@endif
								>
							</label>
						</div>
					</div>
		        </div>
		    @else
		    	<input type="radio" name="menu_type" class="mr-1 ml-2" onclick="show_page();" value="Page">Page
		    	<input type="radio" name="menu_type" class="mr-1 ml-2" onclick="show_url();" value="Url">URL
		        <div class="row">
					<div class="col-xl-6 offset-xl-1">
		                <div class="page hide" id="page">
							<label>Page</label>
							<select class="form-control" name="page">
								<option value="">Select The Page</option>
								@foreach($pages as $page)
									<option value="{{$page->id}}">
										{{$page->name}}
									</option>
								@endforeach
							</select>
		                </div>
					</div>
		        </div>
		        <div class="row">
					<div class="col-xl-6 offset-xl-1">
		                <div class="url hide" id="url">
							<label class="d-flex align-items-center">
								URL:
								<input type="text" name="url" class="form-control ml-2">
							</label>
						</div>
					</div>
		        </div>
		    @endif
		    @error('menu_type')
		        <div class="text-danger">{{ $message }}</div>
		    @enderror
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xl-12">
		<div class="form-group">
		    <label>Display Order:</label>
		    <input type="text" class="form-control" name="order" 
		    value="@if(isset($menu->display_order)){{ $menu->display_order }}@else{{ old('order') }}@endif">
		</div>
		@error('order')
		    <div class="text-danger">{{ $message }}</div>
		@enderror
	</div>
</div>
<div class="row">
	<div class="col-xl-12">
		<div class="form-group">
			<label class="text-muted d-flex align-items-center">
				Status:
				<input type="hidden" name="status" class="mr-1 ml-2" value="0">
				<input type="checkbox" name="status" class="mr-1 ml-2" value="1"
					@if(isset($menu->status))
					    {{'checked'}}
					@endif
				>Published
			</label>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xl-12">
		<div class="form-group">
		    <input type="submit" class="btn btn-info" name="submit" value="Save">
		    <a href="{{ route('menus.index') }}" class="btn btn-success">Back</a>
		</div>
	</div>
</div>