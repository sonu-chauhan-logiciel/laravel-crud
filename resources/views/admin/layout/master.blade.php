<html>
	<head>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>@yield('title')</title>
		<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/adminStyle.css') }}">
		<script src="{{ asset('js/app.js') }}" defer></script>
		<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
	</head>
	<body>
		<section class="dashboard_section">
			<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
			  	<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Company name</a>
				<ul class="navbar-nav px-3">
					<li class="nav-item text-nowrap">
						<a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
						    Logout
						</a> 
						<form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
						    {{ csrf_field() }}
						</form>
					</li>
				</ul>
			</nav>
			<div class="container-fluid">
				<div class="row">
					<nav class="col-md-2 d-none d-md-block bg-light sidebar">
						<div class="sidebar-sticky">
							<ul class="nav flex-column">
								<li class="nav-item">
									<a class="nav-link active" href="{{ url('dashboard') }}">
										<i class="fa fa-tachometer" aria-hidden="true"></i>
										Dashboard
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('user.index') }}">
										<i class="fa fa-folder-o" aria-hidden="true"></i>
										Users
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('posts.index')}}">
										<i class="fa fa-folder-o" aria-hidden="true"></i>
										Posts
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('comment.index')}}">
										<i class="fa fa-folder-o" aria-hidden="true"></i>
										Comments
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('pages.index')}}">
										<i class="fa fa-folder-o" aria-hidden="true"></i>
										Pages
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('menus.index') }}">
										<i class="fa fa-folder-o" aria-hidden="true"></i>
										Menus
									</a>
								</li>
							</ul>
						</div>
					</nav>
					<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
						@yield('content')
					</main>
				</div>
			</div>
		</section>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
		<script>
		   var konten = document.getElementById("content");
		   CKEDITOR.replace(konten,{
		    	language:'en-gb'
		   });
		   CKEDITOR.config.allowedContent = true;
		</script>
		<script type="text/javascript">
			function show_page(){
			  document.getElementById('page').style.display ='block';
			  document.getElementById('url').style.display ='none';
			}
			function show_url(){
			  document.getElementById('url').style.display = 'block';
			  document.getElementById('page').style.display ='none';
			}
		</script>
	</body>
</html>