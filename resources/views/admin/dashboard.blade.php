@extends('admin.layout.master')
@section('title', 'Dashboard')
@section('content')
		
	<div class="d-flex align-items-center pt-3 pb-2 mb-3 border-bottom">
		<h1 class="h2">Dashboard</h1>
	</div>
	<div class="col-md-3">
		<div class="column_one_third">
			<div class="card text-center">
			  <div class="card-body">
			    <h5 class="card-title">
			    	{{$users}} users
			    </h5>
			  </div>
			  <div class="card-footer">
			    <a href="{{ route('user.index') }}">View Details</a>
			  </div>
			</div>
		</div>
	</div>
@endsection