@extends('../admin.layout.master')
@section('title', 'Laravel-Crud')
@section('content')
	<section class="main_section">
		<div class="container">
		    <div class="row">
		    	<div class="col-xl-12">
					<div class="card mt-4">
						<div class="card-header d-flex justify-content-between align-items-center">
							<h4 class="text-info">User Detail</h4>
							<a href="{{ route('user.index') }}" class="btn btn-success">Back</a>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered">
									<tbody>
										<tr>
											<td>User Id</td>
											<td>{{$user->id}}</td>
										</tr>
										<tr>
											<td>User Name</td>
											<td>{{$user->name}}</td>
										</tr>
										<tr>
											<td>User Email</td>
											<td>{{$user->email}}</td>
										</tr>
										<tr>
											<td>User Image</td>
											<td>
												<img src="{{ asset('images').'/' .$user->image }}" alt="..." height="50" width="50" />
											</td>
										</tr>
										<tr>
											<td>User Status</td>
											<td>
	            	            				@if($user->status == 1)
													{{'Active'}}
												@else
													{{'Deactive'}}
			            	            		@endif
											</td>
										</tr>
									</tbody>
								</table>  
							</div>
						</div>
					</div>
		    	</div>
			</div>
		</div>
	</section>
@endsection