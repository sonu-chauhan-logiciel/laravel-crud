@extends('frontend.layout.master')
@section('title', 'Login')
@section('login', '')
@section('content')
	<div class="container mt-4 mb-4">
	    <div class="row d-flex justify-content-center">
	        <div class="col-xl-6">
	            <div class="login_area">
	                @if(session()->has('message'))
	                    <div class="alert alert-danger">
	                        {{ session()->get('message') }}
	                    </div>
	                @endif
	                @if(session()->has('msg'))
	                    <div class="alert alert-success">
	                        {{ session()->get('msg') }}
	                    </div>
	                @endif
	                <div class="card">
	                    <div class="card-body">
	                        <form class="form-signin" action="{{ route('frontendlogin') }}" method="POST">
	                            {{ csrf_field() }}
	                            <div class="form-group">
	                                <label>Email address:</label>
	                                <input type="email" name="email" class="form-control" placeholder="Email address" required autofocus>
	                            </div>
								<div class="form-group">
	                                <label>Password:</label>
	                                <input type="password" name="password" class="form-control" placeholder="Password" required>
	                            </div>
	                            <button class="btn btn-lg btn-info btn-block text-uppercase" type="submit">Login</button>
	                        </form>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection