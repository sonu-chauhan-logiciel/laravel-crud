@extends('frontend.layout.master')
@section('title', 'Post Detail')
@section('content')
    <div id="tooplate_main">
		<div id="content" class="float_l">
        	<div class="post">
            	
                <h2>{{ $post->post_name }}</h2>
                @foreach($post->images as $image)
                    <img src="{{ asset('blog_images/'.$image->images) }}" alt="" />   
                @endforeach       
                <div class="meta">
                    <span class="admin">{{$post->user->name}}</span>
                    <span class="date">{{$post->updated_at}}</span>
                    <span class="comment">{{ $post->post_comments }} Comments</span>
                    <div class="cleaner"></div>
                </div> 
                
                <p>
                    {!! $post->description !!}
                </p>
            </div>
            
            <div id="comment_form">
                <h4>Leave your comment</h4>
                @guest
                    <a href="{{ url('frontendlogin')}}" class="btn btn-info text-dark">Please Sign In To Post Comments</a>
                @else
                    <form action="{{ route('comment.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form_row">
                            <label>Your comment</label><br />
                            <input type="hidden" name="post_id" value="{{$post->id}}">
                            <textarea  name="content" rows="5" cols="5" class="form-control"></textarea>
                            @error('content')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <input type="submit" class="btn btn-primary" name="submit" value="Send" />
                    </form>
                @endguest
                <!--<form action="{{-- route('comment.store') --}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form_row">
                        <label>Your comment</label><br />
                        <textarea  name="content" rows="5" cols="5" class="form-control"></textarea>
                        @error('content')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <input type="submit" class="btn btn-primary" name="submit" value="Send" />
                </form>-->
            </div>
            <div class="cleaner h40"></div>
            <h4>Comments</h4>
                        
              	<div id="comment_section">
                    <ol class="comments first_level">
                        @foreach($post->comments as $comment)
                            <li>
                                <div class="comment_box commentbox1">
                                    <div class="comment_text">
                                        <!--<div class="comment_author">Steven <span class="date">November 25, 2048</span> <span class="time">11:35 pm</span></div>-->
                                        <p>
                                            {!! $comment->content !!}
                                        </p>
                                    </div>
                                    <div class="cleaner"></div>
                                </div>                        
                            </li>
                        @endforeach
                    </ol>
                    <div class="cleaner h20"></div>   
                </div>
                
                <div class="cleaner h20"></div>
            <div class="cleaner"></div>
        </div>
        
        <div id="sidebar" class="float_r">
        	<a href="#"><img src="images/ad_300.jpg" alt="image" /></a>
            
            <div class="cleaner h40"></div>
            
            <h5>Recent Posts</h5>
            <div class="rp_pp">
                <a href="#">Integer venenatis pharetra magna vitae ultrices</a>
                <p>Feb 23, 2048 - 20 Comments</p>
                <div class="cleaner"></div>
            </div>
            <div class="rp_pp">
                <a href="#">Vestibulum quis nulla nunc, nec lobortis nunc.</a>
                <p>Feb 16, 2048 - 20 Comments</p>
                <div class="cleaner"></div>
            </div>
            <div class="rp_pp">
                <a href="#">Pellentesque convallis tristique mauris.</a>
                <p>Feb 10, 2048 - 20 Comments</p>
                <div class="cleaner"></div>
            </div>
            
            <div class="cleaner h40"></div>
            
            <h5>Popular Posts</h5>
            <div class="rp_pp">
                <a href="#">Id tempor odio faucibus et proin pharetra justo.</a>
                <p>Feb 02, 2048 - 20 Comments</p>
                <div class="cleaner"></div>
            </div>
            <div class="rp_pp">
                <a href="#">Etiam Fringilla Sapien quis mauris  vestibulum.</a>
                <p>June 03, 2048 - 20 Comments</p>
                <div class="cleaner"></div>
            </div>
            <div class="rp_pp">
                <a href="#">Sed ac arcu ipsum, ut suscipit neque. </a>
                <p>August 08, 2048 - 20 Comments</p>
                <div class="cleaner"></div>
            </div>
            
            <div class="cleaner h40"></div>
            
        	<h5>Flickr Stream</h5>
            <ul class="flickr_stream">
                <li><a href="#"><img class="image_frame" src="images/tooplate_image_02.png" alt="" /></a></li>
                <li><a href="#"><img class="image_frame" src="images/tooplate_image_03.png" alt="" /></a></li>
                <li class="no_margin_right"><a href="#"><img class="image_frame" src="images/tooplate_image_04.png" alt="" /></a></li>
                <li><a href="#"><img class="image_frame" src="images/tooplate_image_05.png" alt="" /></a></li>
                <li><a href="#"><img class="image_frame" src="images/tooplate_image_06.png" alt="" /></a></li>
                <li class="no_margin_right"><a href="#"><img class="image_frame" src="images/tooplate_image_07.png" alt="" /></a></li>
            </ul>
        </div>        
        
       
        <div class="cleaner"></div>
	</div>
@endsection