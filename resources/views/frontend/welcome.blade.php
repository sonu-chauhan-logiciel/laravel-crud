@extends('frontend.layout.master')
@section('title', 'Blog Post')
@section('content')

    <div id="tooplate_main">
        <div id="content" class="float_l">
            @foreach ($posts as $post)
                <div class="post">
                    <h2>{{ $post->post_name }}</h2>
                    <img src="{{ asset('blog_images/'.$post->singleImage->images) }}" alt="" />  
                    <div class="meta">
                        <span class="admin">{{$post->user->name}}</span>
                        <span class="date">{{$post->updated_at}}</span>
                        <span class="comment">
                            {{$post->post_comments}} Comments
                        </span>
                        <div class="cleaner"></div>
                    </div>
                    
                    <p>
                        {{ $post->short_description }}
                    </p>
                                
                    <span class="more_but"><a href="{{ route('postdetail', ['id' => $post->id]) }}" class="more">More</a></span>
                </div>
            @endforeach
            {{ $posts->links() }}
        <div class="cleaner"></div>
        </div>
        
        <div id="sidebar" class="float_r">
            <a href="#"><img src="images/ad_300.jpg" alt="image" /></a>
            
            <div class="cleaner h40"></div>
            
            <h5>Recent Posts</h5>
            <div class="rp_pp">
                <a href="#">Integer venenatis pharetra magna vitae ultrices</a>
                <p>Feb 23, 2048 - 20 Comments</p>
                <div class="cleaner"></div>
            </div>
            <div class="rp_pp">
                <a href="#">Vestibulum quis nulla nunc, nec lobortis nunc.</a>
                <p>Feb 16, 2048 - 20 Comments</p>
                <div class="cleaner"></div>
            </div>
            <div class="rp_pp">
                <a href="#">Pellentesque convallis tristique mauris.</a>
                <p>Feb 10, 2048 - 20 Comments</p>
                <div class="cleaner"></div>
            </div>
            
            <div class="cleaner h40"></div>
            
            <h5>Popular Posts</h5>
            <div class="rp_pp">
                <a href="#">Id tempor odio faucibus et proin pharetra justo.</a>
                <p>Feb 02, 2048 - 20 Comments</p>
                <div class="cleaner"></div>
            </div>
            <div class="rp_pp">
                <a href="#">Etiam Fringilla Sapien quis mauris  vestibulum.</a>
                <p>June 03, 2048 - 20 Comments</p>
                <div class="cleaner"></div>
            </div>
            <div class="rp_pp">
                <a href="#">Sed ac arcu ipsum, ut suscipit neque. </a>
                <p>August 08, 2048 - 20 Comments</p>
                <div class="cleaner"></div>
            </div>
            
            <div class="cleaner h40"></div>
            
            <h5>Flickr Stream</h5>
            <ul class="flickr_stream">
                <li><a href="#"><img class="image_frame" src="images/tooplate_image_02.png" alt="" /></a></li>
                <li><a href="#"><img class="image_frame" src="images/tooplate_image_03.png" alt="" /></a></li>
                <li class="no_margin_right"><a href="#"><img class="image_frame" src="images/tooplate_image_04.png" alt="" /></a></li>
                <li><a href="#"><img class="image_frame" src="images/tooplate_image_05.png" alt="" /></a></li>
                <li><a href="#"><img class="image_frame" src="images/tooplate_image_06.png" alt="" /></a></li>
                <li class="no_margin_right"><a href="#"><img class="image_frame" src="images/tooplate_image_07.png" alt="" /></a></li>
            </ul>
        </div>        
        
       
        <div class="cleaner"></div>
    </div> 
@endsection
