<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>@yield('title')</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">

        <!-- Favicons -->
        <link href="{{ asset('img/favicon.png') }}" rel="icon">
        <link href="{{ asset('img/apple-touch-icon.png') }}" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">

        <!-- Bootstrap CSS File -->
        <link href="{{ asset('lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

        <!-- Libraries CSS Files -->
        <link href="{{ asset('lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('lib/animate/animate.min.css') }}" rel="stylesheet">
        <link href="{{ asset('lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
        <link href="{{ asset('lib/magnific-popup/magnific-popup.css') }}" rel="stylesheet">
        <link href="{{ asset('lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">

        <!-- Main Stylesheet File -->
        <link href="{{ asset('css/style2.css') }}" rel="stylesheet">
    </head>
    <body id="body">
        <!--==========================
        Top Bar
        ============================-->
        <section id="topbar" class="d-none d-lg-block">
            <div class="container clearfix">
                <div class="contact-info float-left">
                    <i class="fa fa-envelope-o"></i> <a href="mailto:contact@example.com">contact@example.com</a>
                    <i class="fa fa-phone"></i> +1 5589 55488 55
                </div>
                <div class="social-links float-right">
                    <a href="#" class="twitter">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="#" class="facebook">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="#" class="instagram">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a href="#" class="google-plus">
                        <i class="fa fa-google-plus"></i>
                    </a>
                    <a href="#" class="linkedin">
                        <i class="fa fa-linkedin"></i>
                    </a>
                </div>
            </div>
        </section>

        <!--==========================
        Header
        ============================-->
        <header id="header">
            <div class="container">

                <div id="logo" class="pull-left">
                    <h1><a href="{{ url('home') }}" class="scrollto">Port<span>folio</span></a></h1>
                </div>

                <nav id="nav-menu-container">
                    <ul class="nav-menu">
                        @foreach($menus as $menu)
                            @foreach($menu->children as $cat)
                                <li>
                                    @if($cat->ref_type == 'Page')
                                        <a href="{{$cat->page->slug}}">{{$cat->name}}</a>
                                    @else
                                        <a href="{{$cat->ref_value}}" target="_blank">{{$cat->name}}</a>
                                    @endif
                                    @if(!$cat->children->isEmpty())
                                        <ul>
                                            @foreach($cat->children as $submenu)
                                                <li>
                                                    @if($submenu->ref_type == 'Url')
                                                        <a href="{{$submenu->ref_value}}" target="_blank">{{$submenu->name}}</a>
                                                    @else
                                                        <a href="{{$submenu->page->slug}}">{{$submenu->name}}</a>
                                                    @endif
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        @endforeach
                    </ul>
                </nav>
            </div>
        </header><!-- #header -->

        @yield('content')

        <!--==========================
        Footer
        ============================-->
        <footer id="footer">
            <div class="container">
                <div class="copyright">
                    &copy; Copyright <strong>Portfolio</strong>. All Rights Reserved
                </div>
            </div>
        </footer><!-- #footer -->

        <a href="#" class="back-to-top">
            <i class="fa fa-chevron-up"></i>
        </a>

        <!-- JavaScript Libraries -->
        <script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('lib/jquery/jquery-migrate.min.js') }}"></script>
        <script src="{{ asset('lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('lib/easing/easing.min.js') }}"></script>
        <script src="{{ asset('lib/superfish/hoverIntent.js') }}"></script>
        <script src="{{ asset('lib/superfish/superfish.min.js') }}"></script>
        <script src="{{ asset('lib/wow/wow.min.js') }}"></script>
        <script src="{{ asset('lib/owlcarousel/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('lib/magnific-popup/magnific-popup.min.js') }}"></script>
        <script src="{{ asset('lib/sticky/sticky.js') }}"></script>

        <!-- Contact Form JavaScript File -->
        <script src="{{ asset('contactform/contactform.js') }}"></script>

        <!-- Template Main Javascript File -->
        <script src="{{ asset('js/main.js') }}"></script>
    </body>
</html>