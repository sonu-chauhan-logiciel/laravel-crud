<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
    |----------Frontend Login Route Start----------|
*/
    Route::get('/frontendlogin', 'Frontend\LoginController@showLoginForm');
    Route::post('/frontendlogin', 'Frontend\LoginController@login')->name('frontendlogin');
    Route::post('/frontendlogout', 'Frontend\LoginController@logout')->name('frontendlogout');
/*
    |----------Frontend Login Route Start----------|
 */
/*-----------------Portfolio Route Start-----------------*/
    Route::get('/portfolio/{slug}', 'Frontend\frontendController@home');
    /*Route::get('/about-us', 'Frontend\frontendController@about');
    Route::get('/services', 'Frontend\frontendController@services');
    Route::get('/team', 'Frontend\frontendController@team');
    Route::get('/contact-us', 'Frontend\frontendController@contactUs');*/
/*-----------------Portfolio Route End-----------------*/

Route::get('/', 'Frontend\frontendController@index');
Route::get('/post/{id}', 'Frontend\frontendController@postDetail')->name('postdetail');
Route::get('/comment', 'Admin\CommentController@index')->name('comment.index');
Route::post('/comment/', 'Admin\CommentController@store')->name('comment.store');
Route::get('/comment/edit/{id}', 'Admin\CommentController@edit')->name('comments.edit');
Route::put('/{id}', 'Admin\CommentController@update')->name('comments.update');
Route::delete('/delete/{id}', 'Admin\CommentController@destroy')->name('comments.delete');



Route::prefix('users')->name('user.')->group(function () {
	Route::get('/', 'Admin\UsersController@index')->name('index');
    Route::get('/add', 'Admin\UsersController@create')->name('create');
    Route::post('/', 'Admin\UsersController@store')->name('store');
    Route::get('/edit/{id}', 'Admin\UsersController@edit')->name('edit');
    Route::put('/{id}', 'Admin\UsersController@update')->name('update');
    Route::get('/view/{id}', 'Admin\UsersController@show')->name('view');
    Route::delete('/delete/{id}','Admin\UsersController@destroy')->name('delete');
});

Route::prefix('posts')->name('posts.')->group(function () {
    Route::get('/', 'Admin\PostController@index')->name('index');
    Route::get('/addpost', 'Admin\PostController@create')->name('create');
    Route::post('/', 'Admin\PostController@store')->name('store');
    Route::get('/edit/{id}', 'Admin\PostController@edit')->name('edit');
    Route::put('/{id}', 'Admin\PostController@update')->name('update');
    Route::get('/view/{id}', 'Admin\PostController@show')->name('view');
    Route::delete('/delete/{id}','Admin\PostController@destroy')->name('delete');

        Route::prefix('image')->name('image.')->group(function () {
            Route::get('/{id}', 'Admin\ImageController@index')->name('listing');
            Route::post('/{id}', 'Admin\ImageController@store')->name('save');
            Route::delete('/delete/{id}', 'Admin\ImageController@destroy')->name('delete');
            Route::get('/edit/{id}', 'Admin\ImageController@edit')->name('edit');
            Route::put('/{id}', 'Admin\ImageController@update')->name('update');
        });
});
Route::prefix('pages')->name('pages.')->group(function (){
    Route::get('/', 'Admin\PagesController@index')->name('index');
    Route::get('/create', 'Admin\PagesController@create')->name('create');
    Route::post('/', 'Admin\PagesController@store')->name('store');
    Route::get('/edit/{id}', 'Admin\PagesController@edit')->name('edit');
    Route::put('/{id}', 'Admin\PagesController@update')->name('update');
    Route::get('/view{id}', 'Admin\PagesController@show')->name('view');
    Route::delete('/delete/{id}','Admin\PagesController@destroy')->name('delete');
});

Route::prefix('menus')->name('menus.')->group(function (){
    Route::get('/', 'Admin\MenuController@index')->name('index');
    Route::get('/create', 'Admin\MenuController@create')->name('create');
    Route::post('/', 'Admin\MenuController@store')->name('store');
    Route::get('edit/{id}', 'Admin\MenuController@edit')->name('edit');
    Route::put('/{id}', 'Admin\MenuController@update')->name('update');
    Route::delete('/delete/{id}', 'Admin\MenuController@destroy')->name('delete');
    /*Route::get('/', 'Admin\SubmenuController@index')->name('index');
    Route::get('/create', 'Admin\SubmenuController@create')->name('create');
    Route::post('/', 'Admin\SubmenuController@store')->name('store');
    Route::get('edit/{id}', 'Admin\SubmenuController@edit')->name('edit');
    Route::put('/{id}', 'Admin\SubmenuController@update')->name('update');
    Route::delete('/delete/{id}', 'Admin\SubmenuController@destroy')->name('delete');*/
});     


Route::get('/login', 'Admin\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Admin\LoginController@login');
Route::post('/logout', 'Admin\LoginController@logout')->name('logout');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', 'Admin\DashboardController@index');
});

Route::get('/password/reset', 'Admin\ForgotPasswordController@showForm')->name('password.request');
Route::post('password/email', 'Admin\ForgotPasswordController@sendEmail')->name('password.email');
Route::get('password/reset/{token}', 'Admin\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Admin\ResetPasswordController@resetPassword')->name('password.update');

/*Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');*/
