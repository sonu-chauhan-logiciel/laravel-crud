<?php

return [
	'user_profile_image_path' => [
		'original' => 'public/images/',
		'thumbnail' => 'public/images/thumbnail/'
	]
];