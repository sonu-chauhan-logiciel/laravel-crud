<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Post::class,4)->create()->each(function($post)
        	{
	        	$post->images()->saveMany(factory(App\Image::class, rand(1,4))->make());
	        	$post->comments()->saveMany(factory(App\Comment::class, rand(1,5))->make());
        	}
    	);
    }
}
