<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use App\Post;
use App\User;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    
    $userIds = User::pluck('id');
    return [
        'user_id' => $faker->randomElement($userIds), 
        'Content' => $faker->sentence,
        'status' =>1,
        'created_at' => date("Y-m-d h:i:s"),
        'updated_at' => date("Y-m-d h:i:s")
    ];
});
