<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Image;
use Faker\Generator as Faker;

$factory->define(Image::class, function (Faker $faker) {
    return [
        'images' => $faker->image(public_path('blog_images'),200,200, null, false),
        'status' =>1,
        'created_at' => date("Y-m-d h:i:s"),
        'updated_at' => date("Y-m-d h:i:s")
    ];
});
