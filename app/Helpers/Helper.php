<?php

	namespace App\Helpers;
	use Illuminate\Support\Facades\Storage;
	use Image;

	class Helper{

	    public function imageUpload($request)
	    {
	        //image name with Extension
            $imagewithextension = $request->getClientOriginalName();
            //image name without Extension
            $imagename = pathinfo($imagewithextension, PATHINFO_FILENAME);
			//get file extension
            $extension = $request->getClientOriginalExtension();
            $image = $imagename.'_'.uniqid().'.'.$extension;
            $destinationPath = public_path('/images/thumbnail');
            $thumb_img = Image::make($request->getRealPath())->resize(100, 100);
            $thumb_img->save($destinationPath.'/'.$image,80);
            $destinationPath = public_path('/images');
            $request->move($destinationPath, $image);
            return $image;
	    }
	}