<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	
    protected $fillable = [
		'user_id','post_name', 'short_description','description','status'
	];

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function images(){
		return $this->hasMany('App\Image');
	}

	public function singleImage(){
		return $this->hasOne('App\Image')->where('feature_image', 1);
	}

	public function comments(){
		return $this->hasMany('App\Comment');
	}

	public function delete()
    {
        // delete all associated Images And Comments
        $this->images()->delete();
        $this->comments()->delete();
		// delete the post
        return parent::delete();
    }
}
