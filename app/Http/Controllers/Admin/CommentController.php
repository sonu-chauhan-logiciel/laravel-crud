<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CommentStoreRequest;
use App\Repositories\CommentRepository;
use Illuminate\Support\Facades\Auth;
use App\Comment;

class CommentController extends Controller
{	
	protected $commentRepository;

	public function __construct(CommentRepository $comment)
	{
		$this->commentRepository = $comment;
	}

	/**
	 * Display a listing of the Comments.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$comments = $this->commentRepository->get();
		return view('admin.comments.comment', [
			'comments' => $comments
		]);
	}

	/**
	 * Store a newly created Comment in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(CommentStoreRequest $request)
	{
		$user_id = Auth::id();
		$this->commentRepository->store($request, $user_id);
		return redirect()->back();
	}

	/**
	 * Show the form for editing the specified Comment.
	 *
	 * @param  \App\Comment  
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id){
		$comment = $this->commentRepository->edit($id);
		return view('admin.comments.commentEdit', [
			'comment' => $comment
		]);
	}

	/**
	 * Update the specified Comment in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Comment
	 * @return \Illuminate\Http\Response
	 */
	public function update(CommentStoreRequest $request, $id){
		$this->commentRepository->update($request, $id);
		return redirect()->route('comment.index')->with('message', 'comment Updated Successfully');
	}

	/**
	 * Remove the specified Comment from storage.
	 *
	 * @param  \App\Comment
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id){
		$this->commentRepository->delete($id);
		return redirect()->route('comment.index')->with('message', 'comment Deleted Successfully');

	}
}
