<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;

class LoginController extends Controller
{
	/**
	 * Show the form for login User.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function showLoginForm(){
		return view('admin.login');
	}

	/**
	 * Make User Login Request.
	 *@param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */

	public function login(Request $request)
	{
		$email = $request->input('email');
		$password = $request->input('password');
		
		if (Auth::attempt(['email' => $email, 'password' => $password, 'status' => 1], $request->filled('remember'))) {
		    $request->session()->put('email', $email);
		    return redirect()->intended('dashboard');
		}
		return redirect()->route('login')->with('message', 'Invalid credentials');
	}

	/**
	 * User Logout.
	 *@param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function logout(Request $request)
	{
		Auth::logout();
		$request->session()->flash('email');
		return redirect('/login');
	}
}
