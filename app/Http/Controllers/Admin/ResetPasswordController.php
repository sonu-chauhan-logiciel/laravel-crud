<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Repositories\UserRepository;




class ResetPasswordController extends Controller
{
    protected $usersEmail;

    public function __construct(UserRepository $email)
    {
        $this->usersEmail = $email;
    }
    /**
     * Show the form for Reset Password.
     *
     * @return \Illuminate\Http\Response
     */

    public function showResetForm(Request $request, $token = null)
    {
    	return view('admin.resetPassword')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    /**
     * Reset Password.
     *
     * @return \Illuminate\Http\Response
     */
    public function resetPassword(Request $request)
    {
    	$this->validate($request,[
			'token' => 'required',
            'password' => 'required|confirmed|min:8',
		]);
        $email = $this->usersEmail->getEmail($request->token);
		$password = Hash::make($request->password);
		$user = User::where('email', $email)->first();
		$user->password = $password;
		$user->setRememberToken(Str::random(60));
		$user->save();
		return redirect('/login')->with('msg', 'Password Updated');
    }
}
