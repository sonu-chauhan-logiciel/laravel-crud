<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PageStoreRequest;
use App\Repositories\PageRepository;

class PagesController extends Controller
{
	protected $pageRepository;

	public function __construct(PageRepository $page)
	{
		$this->pageRepository = $page;
	}

    /**
	* 
	* Display a listing of the Pages.
	*
	*/
	public function index()
	{
		$pages = $this->pageRepository->get();
		return view('admin.pages.pageListing', [
			'pages' => $pages
		]);
	}

    /**
	* 
	* Show a Form For Create Page.
	*
	*/
	public function create()
	{
		return view('admin.pages.createPage');
	}

    /**
	* 
	* Store Request Into Storage.
	*
	*/
	public function store(PageStoreRequest $request)
	{
		$this->pageRepository->store($request);
		return redirect()->route('pages.index')->with('message', 'Page Store Successfully');
	}

    /**
	* 
	* view Specific Page.
	*
	*/
	public function show($id)
	{
		$page = $this->pageRepository->show($id);
		return view('admin.pages.pageView', [
			'page' => $page
		]);
	}

    /**
	* 
	* Show Specific Data For Editing.
	*
	*/
	public function edit($id)
	{
		$page = $this->pageRepository->edit($id);
		return view('admin.pages.pageEdit', [
			'page' => $page
		]);
	}

    /**
	* 
	* Show Specific Data For Editing.
	*
	*/
	public function update(PageStoreRequest $request, $id)
	{
		$this->pageRepository->update($request, $id);
		return redirect()->route('pages.index')->with('message', 'Page update Successfully');
	}

    /**
	* 
	* Delete Specific Page From Storage.
	*
	*/
	public function destroy($id)
	{
		$this->pageRepository->delete($id);
		return redirect()->route('pages.index')->with('message', 'Page deleted Successfully');
	}
}
