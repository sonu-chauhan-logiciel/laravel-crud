<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostStoreRequest;
use App\Repositories\PostRepository;


class PostController extends Controller
{
	protected $postRepository;

	public function __construct(PostRepository $posts)
	{
	    $this->postRepository = $posts;
	}

	/**
	 * Display a listing of the Posts.
	 *
	 * @return \Illuminate\Http\Response
	 */

    public function index(){
    	$posts = $this->postRepository->paginate();
    	return view('admin.posts.postList',[
    		'posts'=>$posts
    	]);
    }

    /**
     * Show the form for creating a Post.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(){
    	return view('admin.posts.createPost');
    }

    /**
     * Store a newly created Post in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(PostStoreRequest $request){
    	$this->postRepository->store($request);
    	return redirect()->route('posts.index')->with('message', 'Post created successfully');
	}

	/**
	 * Display the specified Post.
	 *
	 * @param  \App\Post  $post
	 * @return \Illuminate\Http\Response
	 */

	/*public function show($id){
		$post = $this->postRepository->show($id);
		return view('admin.posts.postDetail', [
			'post'=>$post
		]);
	}*/

	/**
	 * Show the form for editing the specified Post.
	 *
	 * @param  \App\Post  $post
	 * @return \Illuminate\Http\Response
	 */

	public function edit($id){
		$post = $this->postRepository->edit($id);
		return view('admin.posts.editPost', [
			'post'=>$post
		]); 
	}

	/**
	 * Update the specified Post in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Post  $post
	 * @return \Illuminate\Http\Response
	 */
	public function update(PostStoreRequest $request, $id){
		$this->postRepository->update($id, $request);
		return redirect()->route('posts.index')->with('message', 'Post Updated successfully');
	}

	/**
	 * Remove the specified Post from storage.
	 *
	 * @param  \App\Post  $post
	 * @return \Illuminate\Http\Response
	 */

	public function destroy($id){
		$this->postRepository->delete($id);
		return redirect()->route('posts.index')->with('message', 'Post Deleted successfully');
	}
}
