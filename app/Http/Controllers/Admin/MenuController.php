<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MenuStoreRequest;
use App\Repositories\MenuRepository;
use App\Page;
use App\Menu;


class MenuController extends Controller
{
	protected $menuRepository;
	/**
	 * summary
	 */
	public function __construct(MenuRepository $menus)
	{
	   $this->menuRepository = $menus;
	}
    /**
	* 
	* Display a listing of the Pages.
	*
	*/
	public function index()
	{
		$menus = $this->menuRepository->get();
		return view('admin.menus.list', [
			'menus' => $menus
		]);
	}

    /**
	* 
	* Show Form For Create Menu.
	*
	*/
	public function create()
	{
		$pages = Page::get();
		$menus = Menu::get()->toTree();
		return view('admin.menus.create',compact('pages','menus'));
	}

    /**
	* 
	* Store menus to Storage.
	*
	*/
	public function store(MenuStoreRequest $request)
	{
		$this->menuRepository->store($request);
		return redirect()->route('menus.index')->with('message', 'Menu Created Successfully');
	}

    /**
	* 
	* Show Form For Editing Specific Menu.
	*
	*/
	public function edit($id)
	{
		$pages = Page::get();
		$menus = Menu::get()->toTree();
		$menu = $this->menuRepository->edit($id);
		return view('admin.menus.edit', compact('menu','pages','menus'));
	}

    /**
	* 
	* Update Specific Menu.
	*
	*/
	public function update(MenuStoreRequest $request, $id)
	{
		$this->menuRepository->update($request, $id);
		return redirect()->route('menus.index')->with('message', 'Menu Updated Successfully');
	}

    /**
	* 
	* Update Specific Menu.
	*
	*/
	public function destroy($id)
	{
		$this->menuRepository->delete($id);
		return redirect()->route('menus.index')->with('message', 'Menu Deleted Successfully');
			
	}
}
