<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserStoreRequest;
use App\Repositories\UserRepository;


class UsersController extends Controller
{
    protected $usersRepository;

    public function __construct(UserRepository $users)
    {
        $this->usersRepository = $users;
    }

    /**
     * Display a listing of the Users.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $users = $this->usersRepository->paginate($request);
        return view('admin.users.user',[
            'users'=>$users
        ]);
    }

    /**
     * Show the form for creating a new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.createuser');
    }

    /**
     * Store a newly created User in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $this->usersRepository->store($request);
        return redirect()->route('user.index')->with('message', 'User Registered successfully');
    }

    /**
     * Display the specified User.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_detail = $this->usersRepository->show($id);
        return view('admin.users.userdetail',[
            'user'=>$user_detail
        ]);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_editing = $this->usersRepository->edit($id);
        return view('admin.users.edit',[
            'user'=>$user_editing
        ]);
    }

    /**
     * Update the specified User in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserStoreRequest $request, $id)
    {
        $this->usersRepository->update($id, $request);
        return redirect()->route('user.index')->with('message', 'User Updated successfully');
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->usersRepository->delete($id);
        return redirect()->route('user.index')->with('message', 'User deleted successfully');
    }
}
