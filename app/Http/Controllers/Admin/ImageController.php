<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ImageStoreRequest;
use App\Repositories\ImageRepository;
use App\Post;

class ImageController extends Controller
{

    protected $imageRepository;

    public function __construct(ImageRepository $image)
    {
        $this->imageRepository = $image;
    }
    
    /**
     * Display a listing of the Images In a Post.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
    	$posts = Post::find($id)->images;
        return view('admin.posts.postImage', [
    		'posts' => $posts
    	]);
    }

    /**
     * Store a newly Posted Image in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImageStoreRequest $request, $id)
    {
        $this->imageRepository->store($request, $id);
        return redirect()->back()->with('message', 'Image Uloaded successfully');
    }
    
    /**
     * Remove the specified Post Image from storage.
     *
     * @param  \App\Image
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->imageRepository->delete($id);
        return redirect()->back()->with('message', 'Image Deleted Successfully');
    }
}
