<?php

namespace App\Http\Controllers\Frontend;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;

class LoginController extends Controller
{
	/**
	 * Show the form for login User.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function showLoginForm()
	{
		return view('frontend.login');
	}

	/**
	 * Make User Login Request.
	 *@param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */

	public function login(Request $request)
	{
		if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
		    return Redirect::intended();
		}
		return redirect()->back()->with('message', 'Invalid credentials');
	}

	/**
	 * User Logout.
	 *@param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function logout(Request $request)
	{
		Auth::logout();
		return back();
	}
}
