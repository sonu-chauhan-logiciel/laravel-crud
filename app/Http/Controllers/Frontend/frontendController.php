<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Menu;
use App\Page;

class frontendController extends Controller
{
    
    /**
     * Display a listing of Posts.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $posts = Post::with(['user','singleImage'])->orderby('id','desc')->paginate(2);
    	return view('frontend.welcome',[
    		'posts'=>$posts
    	]);
    }

    /**
     * Display a Deatail of Specified Post.
     *
     * @return \Illuminate\Http\Response
     */
    public function postDetail($id){
		$post = Post::with(['user','images'])->find($id);
		return view('frontend.fullPost',[
			'post'=>$post
		]);  
    }

    /**
     * Display Data.
     *
     * @return \Illuminate\Http\Response
     */
    public function home($slug){
        $menus = Menu::with('page')->orderBy('display_order', 'ASC')->get()->toTree();
        $homePage = Page::where('slug', $slug)->get();
        return view('portfolio',compact('menus','homePage'));
    }
}
