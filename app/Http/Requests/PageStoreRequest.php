<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->getMethod() == 'PUT') 
        {
            return [
                'name' => 'required|string|max:255',
                'short_description' => 'required|string|max:255',
                'description' => 'required',
            ];       
        }
        else
        {
            return [
                'name' => 'required|string|max:255',
                'short_description' => 'required|string|max:255',
                'description' => 'required',
                'status' => 'required',
            ];
        }
    }
}
