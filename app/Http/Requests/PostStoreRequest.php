<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->getMethod() == 'PUT') 
        {
            return [
                'name' => 'required|string|max:255',
                'short_description' => 'required|string|max:255',
                'description' => 'required',
                'status' => 'required',
            ];       
        }
        else
        {
            return [
                'name' => 'required|string|max:255',
                'short_description' => 'required|string|max:255',
                'description' => 'required',
                'status' => 'required',
            ];
        }
    }

    public function messages()
    {
        return [
            'name.required' => 'Post Name is required!',
            'short_description.required' => 'Description is required!',
            'description.required' => 'Description is required!',
            'status.required' => 'Status required',

        ];
    }
}
