<?php

namespace App\Observers;

use App\Page;
use Illuminate\Support\Str;

class PageObserver
{
    /**
     * Handle the page "saving" event.
     *
     * @param  \App\page  $page
     * @return void
     */
    public function saving(Page $page)
    {
        $page->slug = Str::slug($page->name, '-');
    }
}
