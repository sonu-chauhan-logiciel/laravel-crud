<?php

namespace App\Observers;

use App\Comment;
use App\Post;
use DB;

class CommentObserver
{
    /**
     * Handle the comment "saved" event.
     *
     * @param  \App\Comment  $comment
     * @return void
     */
    public function created(Comment $comment)
    {
    	$post_id = $comment->post_id;
        $comments = DB::table('comments')
                    ->where('post_id', $post_id)
                    ->count();
        DB::table('posts')
            ->where('id', $post_id)
            ->update(['post_comments' => $comments]);
    }
}
