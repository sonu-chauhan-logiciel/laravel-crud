<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Submenu extends Model
{
	protected $guarded = [];
	
    use NodeTrait;

	public function page()
    {
        return $this->hasOne('App\Page', 'id', 'ref_value');
    }
}
