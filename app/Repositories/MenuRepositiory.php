<?php

namespace App\Repositories;

use App\Menu;
use Illuminate\Http\Request;

class MenuRepository
{
    /**
	* Display All List Of Menus
	*/
	public function get()
	{
		$result = Menu::get()->where('id', '>',1);
		return $result;
	}

    /**
	* Store Request Into Storage
	*/
	public function store(Request $request)
	{	
		$type_value = ($request->menu_type == "Url") ? $request->url : $request->page;
		$menu = new Menu;
		$menu->parent_id = $request->parent_id;
		$menu->name = $request->name;
		$menu->ref_type = $request->menu_type;
		$menu->ref_value = $type_value;
		$menu->display_order = $request->order;
		$menu->status = $request->status;
		$result = $menu->save();
	}

    /**
	* Find Sepecific page For Editing
	*/
	public function edit($id)
	{
		$result = Menu::find($id);
		return $result;
	}

    /**
	* update Sepecific Menu after Submit Editing Form
	*/
	public function update($request, $id)
	{
		$type_value = ($request->menu_type == "Url") ? $request->url : $request->page;
		$data = [
			'parent_id' => $request->input('parent_id'),
			'name' => $request->input('name'),
			'ref_type' => $request->input('menu_type'),
			'ref_value' => $type_value,
			'display_order' => $request->input('order'),
			'status' => $request->input('status')
		];
		$result = Menu::where('id', $id)->update($data);
		return $result;
	}

    /**
	* Delete Sepecific page from Storage
	*/	
	public function delete($id)
	{
		$menu = Menu::find($id);
		$result = $menu->delete();
		return $result;
	}
}

