<?php

namespace App\Repositories;

use App\Comment;
use Illuminate\Http\Request;

class CommentRepository
{
        /**
    	* Display All List Of Comments
    	*/
    	public function get()
    	{
    		$comment = new Comment;
    		$result = $comment->paginate(7);
    		return $result;
    	}

        /**
    	* Store Request Into Storage
    	*/
    	public function store(Request $request, $user_id)
    	{
    		$comment = new Comment;
    		$comment->post_id = $request->post_id;
    		$comment->user_id = $user_id;
    		$comment->content = $request->content;
    		$comment->status = true;
    		$result = $comment->save();
    		return $result;
    	}


        /**
    	* Find Sepecific Comment For Editing
    	*/
    	public function edit($id)
    	{
    		$result = Comment::find($id);
    		return $result;
    	}

        /**
    	* update Sepecific Comment after Submit Editing Form
    	*/
    	public function update($request, $id)
    	{
    		$data = [
    			'content' => $request->input('content'),
    			'status' => $request->input('status')
    		];
    		$result = Comment::where('id', $id)->update($data);
    		return $result;
    	}

        /**
    	* Delete Sepecific Comment from Storage
    	*/	
    	public function delete($id){
    		$comment = Comment::find($id);
    		$result = $comment->delete();
    		return $result;	
    	}
}