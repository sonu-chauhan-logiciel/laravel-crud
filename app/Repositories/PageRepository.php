<?php

namespace App\Repositories;

use App\Page;
use Illuminate\Http\Request;


class PageRepository
{
    /**
	* Display All List Of Pages
	*/
	public function get()
	{
		$result = Page::get();
		return $result;
	}

    /**
	* Store Request Into Storage
	*/
	public function store(Request $request)
	{
		$page = new Page;
		$page->name = $request->name;
		$page->short_description = $request->short_description;
		$page->description = $request->description;
		$page->status = $request->status;
		$result = $page->save();
		return $result;
	}

    /**
	* View Sepecific Page Data
	*/
	public function show($id)
	{
		$result = Page::findOrfail($id);
		return $result;
	}

    /**
	* Find Sepecific page For Editing
	*/
	public function edit($id)
	{
		$result = Page::find($id);
		return $result;
	}

    /**
	* update Sepecific page after Submit Editing Form
	*/
	public function update($request, $id)
	{
		$data = [
			'name' => $request->name,
			'short_description' => $request->short_description,
			'description' => $request->description,
			'status' => $request->status
		];
		$result = Page::where('id', $id)->update($data);
		return $result;
	}

    /**
	* Delete Sepecific page from Storage
	*/	
	public function delete($id){
		$page = Page::find($id);
		$result = $page->delete();
		return $result;
	}
}
