<?php

namespace App\Repositories;

use App\Image;
use Illuminate\Http\Request;
use File;

class ImageRepository	
{

	/**
	 * Store a newly Posted Image in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	*/
    public function store(Request $request, $id)
    {
    	if ($request->hasfile('image')) {

    	    //image name with Extension
    	    $imagewithextension = $request->file('image')->getClientOriginalName();
    	    //image name without Extension
    	    $imagename = pathinfo($imagewithextension, PATHINFO_FILENAME);
    	    //get file extension
    	    $extension = $request->file('image')->getClientOriginalExtension();
    	    $name = $imagename.'_'.uniqid().'.'.$extension;
    	    $request->file('image')->move(public_path().'/blog_images/', $name);    
    	}
    	$image = new Image;
    	$image->post_id = $id;
    	$image->images = $name;
    	$image->feature_image = $request->featureImage;   
    	$image->status = $request->status;   
    	$result = $image->save();
    	return $result;
    }

    /**
     * Remove the specified Post Image from storage.
     *
     * @param  \App\Image
     * @return \Illuminate\Http\Response
    */
    public function delete($id)
    {
    	$post_image = Image::find($id);
    	$image = $post_image->images;
    	$filename = public_path().'/blog_images/'.$image;
    	File::delete($filename);
    	$result = $post_image->delete();
    	return $result;
    }
}
