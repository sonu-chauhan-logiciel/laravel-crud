<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
	/*public function menu()
    {
        return $this->hasOne('App\Menu', 'ref_value');
    }*/
    public function menu()
    {
        return $this->belongsTo('App\Menu');
    }

    public function submenu()
    {
        return $this->belongsTo('App\Submenu');
    }
}
