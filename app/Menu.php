<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Menu extends Model
{
    protected $guarded = [];
    
    use NodeTrait;

    public function getLftName()
    {
        return 'left';
    }

    public function getRgtName()
    {
        return 'right';
    }

    public function getParentIdName()
    {
        return 'parent_id';
    }

	public function page()
    {
        return $this->hasOne('App\Page', 'id', 'ref_value');
    }
}
